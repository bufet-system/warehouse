package com.infinitycode.bufetsystem.warehouse.model;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity model of Product
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class Product {

  /**
   * Product identifier
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  /**
   * Product type
   */
  @ManyToOne
  ProductType productType;

  /**
   * Date of add product to warehouse
   */
  LocalDate addToWarehouseDate;

  /**
   * Product expiration date
   */
  LocalDate expiryDate;

  /**
   * Product quantity
   */
  double quantity;
}
