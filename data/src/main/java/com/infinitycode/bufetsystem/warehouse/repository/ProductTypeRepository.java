package com.infinitycode.bufetsystem.warehouse.repository;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 08.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.model.Category;
import com.infinitycode.bufetsystem.warehouse.model.ProductType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Repository of managing {@link ProductType} entity
 */
public interface ProductTypeRepository extends CrudRepository<ProductType, Long> {

  /**
   * @param category must be not null
   * @return the list with the given category if none found
   */
  List<ProductType> findProductTypesByCategory(Category category);
}