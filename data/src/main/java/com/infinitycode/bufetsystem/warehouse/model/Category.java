package com.infinitycode.bufetsystem.warehouse.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class Category {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  private String name;

  @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
  private Set<ProductType> productTypeListByGivenCategory;

  private String description;

  @ManyToOne
  private Category parentCategory;

  @OneToMany(mappedBy = "parentCategory", fetch = FetchType.LAZY)
  private Set<Category> subCategory;
}