package com.infinitycode.bufetsystem.warehouse.repository;

import com.infinitycode.bufetsystem.warehouse.model.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Repository of managing {@link Category} entity
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {
  /**
   * Retrieves an entity by its name.
   *
   * @param name must not be {@literal null}.
   * @return list of categories with given name
   */
  List<Category> findByName(String name);
}