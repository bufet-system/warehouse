package com.infinitycode.bufetsystem.warehouse.model;


/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 16.08.19.
 */

public enum UnitsOfMeasurement {
  GRAM,
  DEKAGRAM,
  KILOGRAM,
  LITER,
  MILILITER,
  ITEM,
}
