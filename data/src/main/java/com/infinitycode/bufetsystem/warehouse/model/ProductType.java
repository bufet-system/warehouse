package com.infinitycode.bufetsystem.warehouse.model;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 17.06.19.
 */

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Entity model of Product Type
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ProductType {

  /**
   * Product type identifier
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  /**
   * Product type name
   */
  String name;

  /**
   * Product  type  category
   */
  @ManyToOne
  Category category;

  /**
   * List of product by given product type
   */
  @OneToMany(mappedBy = "productType", fetch = FetchType.LAZY)
  Set<Product> products;

  /**
   * Units Of Measurement
   */
  @Enumerated(EnumType.STRING)
  UnitsOfMeasurement unitsOfMeasurement;
}