# **Buffet-System Issue Description**

# Add new endpoint to microservice

-----------------------------------------------
## Endpoint properties
 
  #### Endpoint description
   "set Endpoint description"
   
  #### URL
   "set url"
   
  #### Request parameters
   "set request parameters"
   
  #### Response
   "set response"
   
  #### Other information's 
   "set other information's"
   
-----------------------------------------------
# Acceptance Criteria

* Create endpoint with property specification
* Create tests 
* Add swagger documentation
* Set Java-Doc
* Set appropriate version
* Code review was accepted 
* Project build without errors
* Sonar check is correct 
* Changelog is updated
* Code merged to master