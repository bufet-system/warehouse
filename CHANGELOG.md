#CHANGELOG
 
 - **version**: 0.13.3
   **issue**: [#34](https://gitlab.com/bufet-system/warehouse/-/issues/34)  
   **description**: add project README
 
 - **version**: 0.13.2  
   **issue**: [general#3](https://gitlab.com/bufet-system/general/-/issues/3)  
   **description**: fix ingress path
    
 - **version**: 0.13.1  
   **issue**: [general#3](https://gitlab.com/bufet-system/general/-/issues/3)  
   **description**: fix deploying configuration for kubernetes/ingress
   
 - [0.13.0]
   [#32] - add pagination and sorting for endpoint to fetch products
   
 - [0.12.2]
   [#38] - change maven repo to artifactory
   
 - [0.12.1]
   [#38] - docker registry path
   
 - [0.12.0]
   [#38] - fix maven deploy
   
 - [0.11.0]
   [#28] - add adCategory service
   [#29] - add removeCategory service
   [#30] - add updateCategoryService 
 - [0.10.0]
   [#31] - add subcategory 
 - [0.9.0]
   [#27] - add getCategories service 
 - [0.8.0]
   [#26] - add changeProductQuantity service 
   
 - [0.7.0]
   [#25] - add removeProduct service 
   
 - [0.6.1]
   [#24] - change package name 
 
 - [0.6.0]
   [#23] -add services for returning productTypes 
 
 - [0.5.2]
   [#21] - add DTO models for getProduct(long id) and getProducts() methods 
   
 - [0.5.1]
   [#17] - add quantity for product
   
 - [0.5.0]
   [#14] - add addProduct service
   
 - [0.4.2]
   [#6] - change test with jUnit to Spock
   
 - [0.4.1]
   [#8] - add dockerfile to project 
   
 - [0.4.0]
   [#5] - add ProductType entity
   
 - [0.3.0]
   [#3] - add getProduct service
   
 - [0.2.0]
   [#4] - change item domains to product
   
 - [0.1.0]
   [#2] - add getItems service
   
 - [0.0.2]
   [#1] - add default project structure
