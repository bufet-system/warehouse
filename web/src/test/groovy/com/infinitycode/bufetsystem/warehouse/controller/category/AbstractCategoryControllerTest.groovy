package com.infinitycode.bufetsystem.warehouse.controller.category

import com.infinitycode.bufetsystem.warehouse.controller.CategoryController
import com.infinitycode.bufetsystem.warehouse.controller.request.CategoryControllerRequests
import com.infinitycode.bufetsystem.warehouse.controller.response.CategoryControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.category.AddCategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.category.AddParentCategoryDto
import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService
import groovy.json.JsonOutput
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class AbstractCategoryControllerTest extends Specification {

  def categoryService = Mock(CategoryService)
  def categoryController = new CategoryController(categoryService)

  protected MockMvc mockMvc

  def setup() {
    mockMvc = standaloneSetup(categoryController).build()
  }

}