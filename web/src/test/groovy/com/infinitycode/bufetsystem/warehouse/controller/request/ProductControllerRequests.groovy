package com.infinitycode.bufetsystem.warehouse.controller.request

import com.infinitycode.bufetsystem.warehouse.util.DateUtils

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 08.08.2019.
 */
class ProductControllerRequests {
  static String ADD_PRODUCT_REQUEST_JSON = '{"product":{"productType":{"id":1},"expiryDate":"' + DateUtils.getNextDateText() + '","quantity":1}}'
  static String CHANGE_PRODUCT_QUANTITY_JSON = '{ "product": { "id": 1, "quantity": 23 } }'
}
