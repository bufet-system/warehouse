package com.infinitycode.bufetsystem.warehouse.service.category


import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse

class GetCategoryServiceTest extends AbstractCategoryServiceTest {

  def "'GetAllCategories' should return correct categories list"() {
    given:

    when:
    GetCategoriesResponse getCategoriesResponse = categoryService.getAllCategories()

    then:
    1 * categoryRepository.findAll() >> [category]

    and:
    getCategoriesResponse.categories == [categoryDto]
  }

}