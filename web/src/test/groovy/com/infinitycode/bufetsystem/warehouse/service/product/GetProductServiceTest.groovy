package com.infinitycode.bufetsystem.warehouse.service.product


import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException
import com.infinitycode.bufetsystem.warehouse.model.Product
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class GetProductServiceTest extends AbstractProductServiceTest {

  def "'GetProduct' should return product"() {
    given:

    when:
    GetProductResponse getProductResponse = productService.getProduct(1L)

    then:
    1 * productRepository.findById(1L) >> [product]

    and:
    getProductResponse.product == getProductDto
  }

  def "'GetProducts' should return valid product Page"() {
    given:
    Pageable pageable = PageRequest.of(0, 2)
    Page<Product> productPage = new PageImpl<>([product, product, product], pageable, 10)

    when:
    GetProductsResponse getProductsResponse = productService.getProducts(pageable)

    then:
    1 * productRepository.findAll(pageable) >> productPage

    and:
    getProductsResponse.getProductsPage().getSize() == 2
    getProductsResponse.getProductsPage().getTotalElements() == 10
    getProductsResponse.getProductsPage().getTotalPages() == 5
    getProductsResponse.getProductsPage().getContent().first() == getProductDto
  }

  def "'GetProduct' should throw #ProductNotExistException"() {
    given:

    when:
    productService.getProduct(1L)

    then:
    1 * productRepository.findById(1L) >> Optional.empty()

    and:
    thrown ProductNotExistException
  }

}