package com.infinitycode.bufetsystem.warehouse.controller.response

class CategoryControllerResponses {

  static String GET_CATEGORIES_RESPONSE_JSON = '{"categories":[{"id":1,"name":"FOOD","description":"FOOD description"}]}'
  static String ADD_CATEGORY_RESPONSE_JSON = '{"id":1}'
}