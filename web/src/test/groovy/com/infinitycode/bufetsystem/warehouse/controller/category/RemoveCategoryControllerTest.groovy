package com.infinitycode.bufetsystem.warehouse.controller.category


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class RemoveCategoryControllerTest extends AbstractCategoryControllerTest {

  def "should success remove category"() {
    when:
    def response = mockMvc.perform(delete("/category/item/1"))

    then:
    1 * categoryService.removeCategory(1L)

    and:
    response.andExpect(status().isOk())
  }
}