package com.infinitycode.bufetsystem.warehouse.controller.product

import com.infinitycode.bufetsystem.warehouse.controller.ProductController
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class AbstractProductControllerTest extends Specification {

  def productService = Mock(ProductService)
  def productController = new ProductController(productService)

  protected MockMvc mockMvc
  protected ProductTypeDto dtoProductType
  protected CategoryDto categoryDto

  def setup() {
    mockMvc = standaloneSetup(productController).build()

    categoryDto = CategoryDto.of(1L, "FOOD", "FOOD description", null)
    dtoProductType = ProductTypeDto.of(1L, "Apple", categoryDto, KILOGRAM)
  }

}