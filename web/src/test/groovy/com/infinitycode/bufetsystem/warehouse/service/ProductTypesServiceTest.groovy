package com.infinitycode.bufetsystem.warehouse.service


import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto
import com.infinitycode.bufetsystem.warehouse.model.ProductType
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository
import com.infinitycode.bufetsystem.warehouse.response.peoducttype.GetProductTypesResponse
import com.infinitycode.bufetsystem.warehouse.service.producttype.ProductTypeService
import com.infinitycode.bufetsystem.warehouse.service.producttype.ProductTypeServiceImpl
import spock.lang.Specification

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.2019.
 */
class ProductTypesServiceTest extends Specification {

  private ProductTypeRepository productTypeRepository = Mock()
  private ProductTypeService productTypeService = new ProductTypeServiceImpl(productTypeRepository)

  private ProductType productType
  private ProductTypeDto dtoProductType

  def setup() {

    productType = ProductType.of(1L, "Apple", null, null, KILOGRAM)

    dtoProductType = ProductTypeDto.of(1L, "Apple", null, KILOGRAM)
  }

  def "'GetProducts' should return correct products list"() {
    given:

    when:
    GetProductTypesResponse getProductTypesResponse = productTypeService.getProductTypes()

    then:
    1 * productTypeRepository.findAll() >> [productType]

    and:
    getProductTypesResponse.productTypes == [dtoProductType]
  }
}