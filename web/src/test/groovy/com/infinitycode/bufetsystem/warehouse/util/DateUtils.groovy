package com.infinitycode.bufetsystem.warehouse.util

import java.time.LocalDate

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 16.08.2019.
 */
class DateUtils {
  static String getNextDateText() {
    LocalDate nextDate = LocalDate.now().plusDays(1)

    return nextDate.toString()
  }
}