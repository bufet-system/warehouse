package com.infinitycode.bufetsystem.warehouse.controller.response

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.2019.
 */
class ProductTypeControllerResponses {
  static String GET_PRODUCT_TYPES_RESPONSE_JSON = '{"productTypes":[{"id":1,"name":"Apple","category":{"id":1,"name":"FOOD","description":"FOOD description","parentCategory":{"id"=2,"name":"Parent","description":"Parent","parentCategory":null}}}]}'
}