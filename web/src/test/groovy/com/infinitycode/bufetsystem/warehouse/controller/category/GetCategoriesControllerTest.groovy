package com.infinitycode.bufetsystem.warehouse.controller.category


import com.infinitycode.bufetsystem.warehouse.controller.response.CategoryControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto
import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse

import static java.util.Collections.singletonList
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class GetCategoriesControllerTest extends AbstractCategoryControllerTest {

  def "should success response list of categories"() {
    given:
    CategoryDto categoryDto = CategoryDto.of(1L, "FOOD", "FOOD description", null)
    GetCategoriesResponse getCategoriesResponse = GetCategoriesResponse.of(singletonList(categoryDto))

    when:
    def response = mockMvc.perform(get("/category/items"))

    then:
    1 * categoryService.getAllCategories() >> getCategoriesResponse

    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(CategoryControllerResponses.GET_CATEGORIES_RESPONSE_JSON))
  }

}