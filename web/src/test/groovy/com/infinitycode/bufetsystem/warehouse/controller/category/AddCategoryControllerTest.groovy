package com.infinitycode.bufetsystem.warehouse.controller.category


import com.infinitycode.bufetsystem.warehouse.controller.request.CategoryControllerRequests
import com.infinitycode.bufetsystem.warehouse.controller.response.CategoryControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.category.AddCategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.category.AddParentCategoryDto
import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse
import groovy.json.JsonOutput
import spock.lang.Unroll

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AddCategoryControllerTest extends AbstractCategoryControllerTest {

  def "should return id of added category which has not parent category"() {
    given:
    AddCategoryResponse addCategoryResponse = AddCategoryResponse.of(1L)

    when:
    def response = mockMvc.perform(post("/category/item")
            .contentType(APPLICATION_JSON)
            .content(CategoryControllerRequests.ADD_CATEGORY_REQUEST_WITHOUT_PARENT_CATEGORY_JSON))

    then:
    1 * categoryService.addCategory(_ as AddCategoryRequest) >> addCategoryResponse

    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(CategoryControllerResponses.ADD_CATEGORY_RESPONSE_JSON))
  }

  @Unroll
  def "should return BAD_REQUEST status when request to add category is incorrect"() {
    given:
    AddCategoryRequest addCategoryRequest = AddCategoryRequest.of(AddCategoryDto.of(NAME, DESCRIPTION, PARENT_CATEGORY))

    when:
    def response = mockMvc.perform(post("/category/item")
            .contentType(APPLICATION_JSON)
            .content(JsonOutput.toJson(addCategoryRequest)))

    then:
    response.andExpect(status().isBadRequest())

    where:
    NAME   | DESCRIPTION   | PARENT_CATEGORY
    null   | null          | AddParentCategoryDto.of(1L)
    null   | "Description" | null
    "FOOD" | null          | null
    "FOOD" | "Description" | AddParentCategoryDto.of(null)
  }

}