package com.infinitycode.bufetsystem.warehouse.controller.response

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 30.06.2019.
 */
class ProductControllerResponses {
  static String GET_PRODUCT_RESPONSE_JSON = '{"product":{"id":1,"productType":{"id":1,"name":"Apple","category":{"id":1,"name":"FOOD","description":"FOOD description"}},"addToWarehouseDate":[2019,7,14],"expiryDate":[2019,9,14]}}'
  static String ADD_PRODUCT_RESPONSE_JSON = '{"id":1}'
  static String GET_PRODUCTS_RESPONSE_JSON = '{"productsPage":{"content":[{"id":1,"productType":{"id":1,"name":"Apple","category":{"id":1,"name":"FOOD","description":"FOOD description"}},"expiryDate":[2019,9,14]}],"pageable":{"sort":{"unsorted":true,"sorted":false,"empty":true},"offset":0,"pageSize":3,"pageNumber":0,"unpaged":false,"paged":true},"last":true,"totalPages":1,"totalElements":1,"size":3,"number":0,"sort":{"unsorted":true,"sorted":false,"empty":true},"numberOfElements":1,"first":true,"empty":false}}'
}