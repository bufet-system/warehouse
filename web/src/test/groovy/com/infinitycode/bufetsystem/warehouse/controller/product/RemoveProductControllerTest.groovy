package com.infinitycode.bufetsystem.warehouse.controller.product


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class RemoveProductControllerTest extends AbstractProductControllerTest {

  def "should success remove product"() {
    when:
    def response = mockMvc.perform(delete("/products/1"))

    then:
    1 * productService.removeProduct(1L)

    and:
    response.andExpect(status().isOk())
  }
}