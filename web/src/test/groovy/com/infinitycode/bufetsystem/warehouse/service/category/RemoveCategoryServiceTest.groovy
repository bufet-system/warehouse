package com.infinitycode.bufetsystem.warehouse.service.category


import com.infinitycode.bufetsystem.warehouse.exception.CategoryIsUsedException
import com.infinitycode.bufetsystem.warehouse.exception.CategoryNotExistException
import com.infinitycode.bufetsystem.warehouse.model.Category

import static java.util.Collections.emptyList

class RemoveCategoryServiceTest extends AbstractCategoryServiceTest {

  def "'RemoveCategory' should no error thrown"() {
    given:

    when:
    categoryService.removeCategory(1L)

    then:
    1 * categoryRepository.findById(1L) >> [category]
    1 * productTypeRepository.findProductTypesByCategory(_ as Category) >> emptyList()

    and:
    noExceptionThrown()
  }

  def "'RemoveCategory' should throw #CategoryNotExistException"() {
    given:

    when:
    categoryService.removeCategory(1L)

    then:
    1 * categoryRepository.findById(1L) >> Optional.empty()

    and:
    thrown CategoryNotExistException
  }

  def "'RemoveCategory' should throw #CategoryIsUsedException"() {
    given:

    when:
    categoryService.removeCategory(1L)

    then:
    1 * categoryRepository.findById(1L) >> [category]
    1 * productTypeRepository.findProductTypesByCategory(category) >> [productType]

    and:
    thrown CategoryIsUsedException
  }

}