package com.infinitycode.bufetsystem.warehouse.controller.product


import com.infinitycode.bufetsystem.warehouse.controller.response.ProductControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse
import org.springframework.data.domain.*
import org.springframework.data.web.PageableHandlerMethodArgumentResolver

import java.time.LocalDate

import static java.util.Collections.singletonList
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class GetProductsControllerTest extends AbstractProductControllerTest {

  def "should success paginate response with products "() {
    mockMvc = standaloneSetup(productController)
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();

    given:
    GetProductDto getProductDto = GetProductDto.of((1L), dtoProductType, LocalDate.parse("2019-07-14"), LocalDate.parse("2019-09-14"), 1)
    Page<GetProductResponse> page = new PageImpl<>(singletonList(getProductDto), PageRequest.of(0, 3), 1) as Page<GetProductResponse>
    Sort sort = Sort.by("id").descending();
    Pageable pageRequest = PageRequest.of(0, 3, sort);

    and:
    GetProductsResponse getProductsResponse = GetProductsResponse.of(page as Page<GetProductDto>)

    when:
    def response = mockMvc.perform(get("/products/")
            .param("page", "0")
            .param("size", "3")
            .param("sort", "id,desc"))

    then:
    1 * productService.getProducts(pageRequest) >> getProductsResponse


    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(ProductControllerResponses.GET_PRODUCTS_RESPONSE_JSON))

  }

  def "should success response product"() {
    given:
    GetProductDto getProductDto = GetProductDto.of((1L), dtoProductType, LocalDate.parse("2019-07-14"), LocalDate.parse("2019-09-14"), 1)

    and:
    GetProductResponse getProductResponse = GetProductResponse.of(getProductDto)

    when:
    def response = mockMvc.perform(get("/products/1"))

    then:
    1 * productService.getProduct(1L) >> getProductResponse

    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(ProductControllerResponses.GET_PRODUCT_RESPONSE_JSON))
  }

}