package com.infinitycode.bufetsystem.warehouse.service.category


import com.infinitycode.bufetsystem.warehouse.exception.CategoryNameUniqueError
import com.infinitycode.bufetsystem.warehouse.exception.CategoryNotInsertedException
import com.infinitycode.bufetsystem.warehouse.exception.ParentCategoryNotExistException
import com.infinitycode.bufetsystem.warehouse.model.Category
import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse

import static java.util.Collections.emptyList

class AddCategoryServiceTest extends AbstractCategoryServiceTest {


  def "'AddCategory' should return id of added category"() {
    given:

    when:
    1 * categoryRepository.findByName(_ as String) >> emptyList()

    and:
    AddCategoryResponse addCategoryResponse = categoryService.addCategory(AddCategoryRequest.of(addCategoryDto))

    then:
    1 * categoryRepository.save(_ as Category) >> category

    and:
    addCategoryResponse.id == 1L
  }

  def "'AddCategory' should throw #CategoryNameUniqueError"() {
    given:

    when:
    categoryService.addCategory(AddCategoryRequest.of(addCategoryDto))

    then:
    1 * categoryRepository.findByName(_ as String) >> [category]

    and:
    thrown CategoryNameUniqueError
  }

  def "'AddCategory' should throw #CategoryNotInsertedException"() {
    given:
    when:
    categoryService.addCategory(AddCategoryRequest.of(addCategoryDto))

    then:
    1 * categoryRepository.findByName(_ as String) >> emptyList()

    and:

    thrown CategoryNotInsertedException
  }

  def "'AddCategory' should throw #ParentCategoryNotExistException"() {
    given:

    when:
    categoryService.addCategory(AddCategoryRequest.of(addCategoryDtoWithParentCategory))

    then:

    1 * categoryRepository.findById(34L) >> Optional.empty()
    1 * categoryRepository.findByName(_ as String) >> emptyList()

    and:
    thrown ParentCategoryNotExistException
  }

}