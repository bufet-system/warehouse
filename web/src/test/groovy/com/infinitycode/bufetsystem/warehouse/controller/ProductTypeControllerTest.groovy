package com.infinitycode.bufetsystem.warehouse.controller

import com.infinitycode.bufetsystem.warehouse.controller.response.ProductTypeControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto
import com.infinitycode.bufetsystem.warehouse.response.peoducttype.GetProductTypesResponse
import com.infinitycode.bufetsystem.warehouse.service.producttype.ProductTypeService
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM
import static java.util.Collections.singletonList
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class ProductTypeControllerTest extends Specification {

  def productTypeService = Mock(ProductTypeService)
  def productTypeController = new ProductTypeController(productTypeService)
  private CategoryDto categoryDto
  private CategoryDto parentCategoryDto

  private MockMvc mockMvc

  def setup() {
    mockMvc = standaloneSetup(productTypeController).build()
    parentCategoryDto = CategoryDto.of(2L, "Parent", "Parent", null)
    categoryDto = CategoryDto.of(1L, "FOOD", "FOOD description", parentCategoryDto)

  }

  def "should success response list of products"() {
    given:
    ProductTypeDto productTypeDto = ProductTypeDto.of(1L, "Apple", categoryDto, KILOGRAM)
    GetProductTypesResponse getProductsResponse = GetProductTypesResponse.of(singletonList(productTypeDto))

    when:
    def response = mockMvc.perform(get("/productType/items"))

    then:
    1 * productTypeService.getProductTypes() >> getProductsResponse

    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(ProductTypeControllerResponses.GET_PRODUCT_TYPES_RESPONSE_JSON))
  }
}