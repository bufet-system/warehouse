package com.infinitycode.bufetsystem.warehouse.controller.product


import com.infinitycode.bufetsystem.warehouse.controller.request.ProductControllerRequests
import com.infinitycode.bufetsystem.warehouse.dto.product.ChangeProductQuantityDto
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest
import groovy.json.JsonOutput
import spock.lang.Unroll

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class ChangeQuantityProductControllerTest extends AbstractProductControllerTest {

  def 'should success change product quantity'() {
    when:
    def response = mockMvc.perform(put("/products/quantity")
            .contentType(APPLICATION_JSON)
            .content(ProductControllerRequests.CHANGE_PRODUCT_QUANTITY_JSON))


    then:
    1 * productService.changeProductQuantity(_ as ChangeProductQuantityRequest)

    and:
    response.andExpect(status().isOk())
  }

  @Unroll
  def "should return BAD_REQUEST status when request to update quantity is incorrect"() {
    given:
    ChangeProductQuantityRequest changeProductQuantityRequest = ChangeProductQuantityRequest.of(ChangeProductQuantityDto.of(ID, QUANTITY))

    when:
    def response = mockMvc.perform(put("/products/quantity")
            .contentType(APPLICATION_JSON)
            .content(JsonOutput.toJson(changeProductQuantityRequest)))

    then:
    response.andExpect(status().isBadRequest())

    where:
    ID   | QUANTITY
    1L   | -3
    null | 1.0
  }
}