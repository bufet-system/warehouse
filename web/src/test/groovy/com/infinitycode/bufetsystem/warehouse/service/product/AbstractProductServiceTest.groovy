package com.infinitycode.bufetsystem.warehouse.service.product


import com.infinitycode.bufetsystem.warehouse.dto.product.AddProductDto
import com.infinitycode.bufetsystem.warehouse.dto.product.ChangeProductQuantityDto
import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.AddProductTypeDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto
import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.ProductTypeNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.UpdateProductQuantityError
import com.infinitycode.bufetsystem.warehouse.model.Product
import com.infinitycode.bufetsystem.warehouse.model.ProductType
import com.infinitycode.bufetsystem.warehouse.repository.ProductRepository
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService
import com.infinitycode.bufetsystem.warehouse.service.product.ProductServiceImpl
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Specification

import java.time.LocalDate

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class AbstractProductServiceTest extends Specification {

  protected ProductRepository productRepository = Mock()
  protected ProductTypeRepository productTypeRepository = Mock()
  protected ProductService productService = new ProductServiceImpl(productRepository, productTypeRepository)

  protected Product product
  protected ProductType productType
  protected ProductTypeDto dtoProductType

  protected GetProductDto getProductDto
  protected AddProductDto addProductDto
  protected ChangeProductQuantityDto changeProductQuantityDto


  def setup() {

    productType = ProductType.of(1L, "Apple", null, null, KILOGRAM)
    product = Product.of((1L), productType, LocalDate.parse("2019-07-14"), LocalDate.parse("2019-09-14"), 1)

    dtoProductType = ProductTypeDto.of(1L, "Apple", null, KILOGRAM)

    getProductDto = GetProductDto.of((1L), dtoProductType, LocalDate.parse("2019-07-14"), LocalDate.parse("2019-09-14"), 1)
    addProductDto = AddProductDto.of(AddProductTypeDto.of(1L), LocalDate.parse("2019-07-14"), 1)
    changeProductQuantityDto = ChangeProductQuantityDto.of(1L, 23)

  }
}