package com.infinitycode.bufetsystem.warehouse.service.product

import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class RemoveProductServiceTest extends AbstractProductServiceTest {

  def "'RemoveProduct' should throw #ProductNotExistException"() {
    given:

    when:
    productService.removeProduct(1L)

    then:
    1 * productRepository.findById(1L) >> Optional.empty()

    and:
    thrown ProductNotExistException
  }

  def "'Remove product' should no error thrown"() {
    given:

    when:
    productService.removeProduct(1L)

    then:
    1 * productRepository.findById(1L) >> [product]

    and:
    noExceptionThrown()
  }

}