package com.infinitycode.bufetsystem.warehouse.controller.request

class CategoryControllerRequests {
  static ADD_CATEGORY_REQUEST_WITHOUT_PARENT_CATEGORY_JSON = '{"category":{"name":"Fruit","description":"Fruit category"}}'
  static ADD_CATEGORY_REQUEST_WITH_PARENT_CATEGORY_JSON = '{"category":{"name":"Fruit","description":"Fruit category","parentCategory":{"id":1}}}'
  static UPDATE_CATEGORY_REQUEST_JSON = '{"category":{"id":34,"name":"NEW FOOD","description":"NEW DESCRIPTION"}}'
}