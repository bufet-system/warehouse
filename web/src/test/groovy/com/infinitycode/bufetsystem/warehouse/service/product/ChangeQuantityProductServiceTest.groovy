package com.infinitycode.bufetsystem.warehouse.service.product


import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.UpdateProductQuantityError
import com.infinitycode.bufetsystem.warehouse.model.Product
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class ChangeQuantityProductServiceTest extends AbstractProductServiceTest {

  def "'ChangeProductQuantity' should throw #ProductNotExistException"() {
    given:

    when:
    productService.changeProductQuantity(ChangeProductQuantityRequest.of(changeProductQuantityDto))

    then:
    1 * productRepository.findById(1L) >> Optional.empty()

    and:
    thrown ProductNotExistException
  }

  def "'ChangeProductQuantity' should no error thrown"() {
    given:

    when:
    productService.changeProductQuantity(ChangeProductQuantityRequest.of(changeProductQuantityDto))

    then:
    1 * productRepository.findById(1L) >> [product]
    1 * productRepository.save(_ as Product) >> product

    and:
    noExceptionThrown()
  }

  def "'ChangeProductQuantity' should throw #UpdateProductQuantityError"() {
    given:

    when:
    productService.changeProductQuantity(ChangeProductQuantityRequest.of(changeProductQuantityDto))

    then:
    1 * productRepository.findById(1L) >> [product]
    1 * productRepository.save(_ as Product) >> { throw new Exception() }

    and:
    thrown UpdateProductQuantityError
  }
}