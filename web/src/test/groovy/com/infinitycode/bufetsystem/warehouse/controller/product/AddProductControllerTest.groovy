package com.infinitycode.bufetsystem.warehouse.controller.product


import com.infinitycode.bufetsystem.warehouse.controller.request.ProductControllerRequests
import com.infinitycode.bufetsystem.warehouse.controller.response.ProductControllerResponses
import com.infinitycode.bufetsystem.warehouse.dto.product.AddProductDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.AddProductTypeDto
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse
import groovy.json.JsonOutput
import spock.lang.Unroll

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class AddProductControllerTest extends AbstractProductControllerTest {


  def "should return id of added product"() {
    given:
    AddProductsResponse addProductsResponse = AddProductsResponse.of(1L)

    when:
    def response = mockMvc.perform(post("/products/")
            .contentType(APPLICATION_JSON)
            .content(ProductControllerRequests.ADD_PRODUCT_REQUEST_JSON))

    then:
    1 * productService.addProduct(_ as AddProductRequest) >> addProductsResponse

    and:
    response.andExpect(status().isOk())
            .andExpect(content().json(ProductControllerResponses.ADD_PRODUCT_RESPONSE_JSON))
  }

  @Unroll
  def "should return BAD_REQUEST status during adding product"() {
    given:
    AddProductRequest request = AddProductRequest.of(AddProductDto.of(PRODUCT_TYPE, null, QUANTITY))

    when:
    def response = mockMvc.perform(post("/products/")
            .contentType(APPLICATION_JSON)
            .content(JsonOutput.toJson(request)))

    then:
    response.andExpect(status().isBadRequest())

    where:
    PRODUCT_TYPE               | QUANTITY
    AddProductTypeDto.of(null) | 1
    null                       | 1
    AddProductTypeDto.of(1)    | 0
  }

}