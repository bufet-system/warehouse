package com.infinitycode.bufetsystem.warehouse.service.product


import com.infinitycode.bufetsystem.warehouse.dto.product.AddProductDto
import com.infinitycode.bufetsystem.warehouse.dto.product.ChangeProductQuantityDto
import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.AddProductTypeDto
import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto
import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.ProductTypeNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.UpdateProductQuantityError
import com.infinitycode.bufetsystem.warehouse.model.Product
import com.infinitycode.bufetsystem.warehouse.model.ProductType
import com.infinitycode.bufetsystem.warehouse.repository.ProductRepository
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService
import com.infinitycode.bufetsystem.warehouse.service.product.ProductServiceImpl
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Specification

import java.time.LocalDate

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 29.06.2019.
 */
class AddProductServiceTest extends AbstractProductServiceTest {

  def "'AddProduct' should return product id of added product"() {
    given:

    when:
    AddProductsResponse addProductsResponse = productService.addProduct(AddProductRequest.of(addProductDto))

    then:
    1 * productTypeRepository.findById(1L) >> Optional.ofNullable(productType)
    1 * productRepository.save(_ as Product) >> product

    and:
    addProductsResponse.id == 1L
  }

  def "'AddProduct' should throw #ProductTypeNotExistException"() {
    given:

    when:
    productService.addProduct(AddProductRequest.of(addProductDto))

    then:
    1 * productTypeRepository.findById(1L) >> Optional.empty()

    and:
    thrown ProductTypeNotExistException
  }
}