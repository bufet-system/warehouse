package com.infinitycode.bufetsystem.warehouse.service.category


import com.infinitycode.bufetsystem.warehouse.exception.CategoryNotExistException
import com.infinitycode.bufetsystem.warehouse.exception.UpdateCategoryError
import com.infinitycode.bufetsystem.warehouse.model.Category
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest

import static java.util.Collections.emptyList

class UpdateCategoryServiceTest extends AbstractCategoryServiceTest {

  def "'UpdateCategory' should throw #CategoryNotExistException"() {
    given:

    when:
    categoryService.updateCategory(UpdateCategoryRequest.of(updateCategoryDto))

    then:
    1 * categoryRepository.findById(1L) >> Optional.empty()

    and:
    thrown CategoryNotExistException
  }

  def "'UpdateCategory' should no error thrown"() {
    given:

    when:
    categoryService.updateCategory(UpdateCategoryRequest.of(updateCategoryDto))

    then:
    1 * categoryRepository.findById(1L) >> [category]
    1 * categoryRepository.findByName(_ as String) >> emptyList()
    1 * categoryRepository.save(_ as Category) >> category

    and:
    noExceptionThrown()
  }

  def "'UpdateCategory' should throw #UpdateCategoryError"() {
    given:

    when:
    categoryService.updateCategory(UpdateCategoryRequest.of(updateCategoryDto))

    then:
    1 * categoryRepository.findById(1L) >> [category]
    1 * categoryRepository.findByName(_ as String) >> emptyList()
    1 * categoryRepository.save(_ as Category) >> { throw new Exception() }

    and:
    thrown UpdateCategoryError
  }
}