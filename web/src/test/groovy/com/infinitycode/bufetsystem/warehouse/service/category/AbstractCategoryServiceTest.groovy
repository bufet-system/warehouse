package com.infinitycode.bufetsystem.warehouse.service.category

import com.infinitycode.bufetsystem.warehouse.dto.category.AddCategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.category.AddParentCategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto
import com.infinitycode.bufetsystem.warehouse.dto.category.UpdateCategoryDto
import com.infinitycode.bufetsystem.warehouse.exception.*
import com.infinitycode.bufetsystem.warehouse.model.Category
import com.infinitycode.bufetsystem.warehouse.model.ProductType
import com.infinitycode.bufetsystem.warehouse.repository.CategoryRepository
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository
import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse
import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryServiceImpl
import spock.lang.Specification

import static com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement.KILOGRAM
import static java.util.Collections.emptyList

class AbstractCategoryServiceTest extends Specification {

  protected CategoryRepository categoryRepository = Mock()
  protected ProductTypeRepository productTypeRepository = Mock()
  protected CategoryService categoryService = new CategoryServiceImpl(categoryRepository, productTypeRepository)

  protected Category category
  protected CategoryDto categoryDto
  protected AddCategoryDto addCategoryDto
  protected AddParentCategoryDto parentCategoryDto
  protected AddCategoryDto addCategoryDtoWithParentCategory
  protected UpdateCategoryDto updateCategoryDto
  protected ProductType productType

  def setup() {
    category = Category.of(1L, "FOOD", null, "FOOD description", null, null)
    categoryDto = CategoryDto.of(1L, "FOOD", "FOOD description", null)
    addCategoryDto = AddCategoryDto.of("FOOD", "description", null)
    parentCategoryDto = AddParentCategoryDto.of(34L)
    addCategoryDtoWithParentCategory = AddCategoryDto.of("MEAT", "MEAT description", parentCategoryDto)
    updateCategoryDto = UpdateCategoryDto.of(1L, "NEW FOOD", "New description")
    productType = ProductType.of(1L, "Apple", category, null, KILOGRAM)

  }

}