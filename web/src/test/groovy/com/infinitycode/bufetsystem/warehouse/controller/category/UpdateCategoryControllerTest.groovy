package com.infinitycode.bufetsystem.warehouse.controller.category


import com.infinitycode.bufetsystem.warehouse.controller.request.CategoryControllerRequests
import com.infinitycode.bufetsystem.warehouse.dto.category.UpdateCategoryDto
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest
import groovy.json.JsonOutput
import spock.lang.Unroll

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UpdateCategoryControllerTest extends AbstractCategoryControllerTest {

  def 'should success update category'() {
    when:
    def response = mockMvc.perform(put("/category/item")
            .contentType(APPLICATION_JSON)
            .content(CategoryControllerRequests.UPDATE_CATEGORY_REQUEST_JSON))

    then:
    1 * categoryService.updateCategory(_ as UpdateCategoryRequest)

    and:
    response.andExpect(status().isOk())
  }

  @Unroll
  def "should return BAD_REQUEST status when request to update category is incorrect"() {
    given:
    UpdateCategoryRequest updateCategoryRequest = UpdateCategoryRequest.of(UpdateCategoryDto.of(ID, NAME, DESCRIPTION))

    when:
    def response = mockMvc.perform(put("/category/item")
            .contentType(APPLICATION_JSON)
            .content(JsonOutput.toJson(updateCategoryRequest)))

    then:
    response.andExpect(status().isBadRequest())

    where:
    ID   | NAME   | DESCRIPTION
    1L   | null   | "description"
    2L   | "name" | null
    null | "name" | "description"
  }
}