package com.infinitycode.bufetsystem.warehouse.mapper;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.product.AddProductDto;
import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto;
import com.infinitycode.bufetsystem.warehouse.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import javax.validation.constraints.NotEmpty;

/**
 * MapStruct mapper to map between Product and ProductDto models
 */
@Mapper
public interface ProductMapper {

  /**
   * Map {@link Product} model to {@link GetProductDto} model
   *
   * @param product Product model to mapping
   * @return {@link GetProductDto} model
   */
  GetProductDto mapToGetProductDto(Product product);

  /**
   * Map {@link AddProductDto} model to {@link Product} model
   *
   * @param addProductDto AddProductDtoModel model to mapping
   * @return {@link Product} model
   */
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "expiryDate", ignore = true)
  Product mapToAddProductDto(@NotEmpty AddProductDto addProductDto);
}