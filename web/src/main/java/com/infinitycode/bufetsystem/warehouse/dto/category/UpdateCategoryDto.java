package com.infinitycode.bufetsystem.warehouse.dto.category;

import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Dto Category  model for service: {@link CategoryService#updateCategory(UpdateCategoryRequest)}
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@ApiModel
public class UpdateCategoryDto {

  @NotNull
  @ApiModelProperty(position = 1, required = true, value = "Category id")
  Long id;

  @NotNull
  @ApiModelProperty(position = 2, required = true, value = "Category name")
  String name;

  @NotNull
  @ApiModelProperty(position = 3, required = true, value = "Category description")
  String description;
}