package com.infinitycode.bufetsystem.warehouse.request;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 22.07.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.product.AddProductDto;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Request model of {@link ProductService#addProduct(AddProductRequest)} ()} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
@NoArgsConstructor
public class AddProductRequest {

  @Valid
  @NotNull(message = "product.required")
  @ApiModelProperty(required = true, value = "Product to add")
  private AddProductDto product;
}
