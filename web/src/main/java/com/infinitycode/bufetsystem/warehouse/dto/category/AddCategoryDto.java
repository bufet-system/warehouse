package com.infinitycode.bufetsystem.warehouse.dto.category;

import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Dto Category type model for service: {@link CategoryService#addCategory(AddCategoryRequest)}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
@Builder
public class AddCategoryDto {


  @NotNull(message = "name.required")
  @ApiModelProperty(position = 1, required = true, value = "Category name")
  private String name;

  @NotNull(message = "description.required")
  @ApiModelProperty(position = 2, required = true, value = "Category description")
  private String description;

  @Valid
  @ApiModelProperty(position = 3, value = "Parent category")
  private AddParentCategoryDto parentCategory;
}
