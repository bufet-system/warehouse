package com.infinitycode.bufetsystem.warehouse.response.product;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 08.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Response model of {@link ProductService#addProduct(AddProductRequest)} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
public class AddProductsResponse {
  @ApiModelProperty(required = true, value = "Id of added product")
  private Long id;
}
