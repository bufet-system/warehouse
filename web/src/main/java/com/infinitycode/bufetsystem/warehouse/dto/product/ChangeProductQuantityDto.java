package com.infinitycode.bufetsystem.warehouse.dto.product;


import com.infinitycode.bufetsystem.warehouse.model.Product;
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Dto Product model for service: {@link ProductService#changeProductQuantity(ChangeProductQuantityRequest)} ()}
 * Reference to {@link Product}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class ChangeProductQuantityDto {

  @NotNull
  @ApiModelProperty(position = 1, required = true, value = "Product identifier")
  private Long id;

  @NotNull
  @ApiModelProperty(position = 2, required = true, value = "Product quantity")
  @Min(value = 0, message = "Quantity must be greater than or equals 0")
  private double quantity;
}
