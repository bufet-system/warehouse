package com.infinitycode.bufetsystem.warehouse.service.product;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto;
import com.infinitycode.bufetsystem.warehouse.exception.ProductNotExistException;
import com.infinitycode.bufetsystem.warehouse.exception.ProductNotInserted;
import com.infinitycode.bufetsystem.warehouse.exception.ProductTypeNotExistException;
import com.infinitycode.bufetsystem.warehouse.exception.UpdateProductQuantityError;
import com.infinitycode.bufetsystem.warehouse.mapper.ProductMapper;
import com.infinitycode.bufetsystem.warehouse.model.Product;
import com.infinitycode.bufetsystem.warehouse.model.ProductType;
import com.infinitycode.bufetsystem.warehouse.repository.ProductRepository;
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository;
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest;
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Optional;

/**
 * {@inheritDoc}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;
  private final ProductTypeRepository productTypeRepository;

  private ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

  /**
   * {@inheritDoc}
   */
  @Override
  public GetProductsResponse getProducts(Pageable pageable) {

    log.info("Try to find products with page number: {}, and page size: {}", pageable.getPageNumber(), pageable.getPageSize());
    Page<GetProductDto> getProductDtoPage = productRepository.findAll(pageable)
            .map(productMapper::mapToGetProductDto);

    log.info("Return page number: {}, with page size: {}", getProductDtoPage.getNumber(), getProductDtoPage.getSize());
    return GetProductsResponse.of(getProductDtoPage);
  }

  /**
   * {@inheritDoc}
   */
  @SneakyThrows
  @Override
  public GetProductResponse getProduct(@NotNull long id) {
    GetProductDto getProductDto = productRepository.findById(id)
            .map(productMapper::mapToGetProductDto)
            .orElseThrow(() -> new ProductNotExistException(id));

    log.info("Return product: {} for given id: {}", getProductDto.toString(), id);
    return GetProductResponse.of(getProductDto);
  }

  /**
   * {@inheritDoc}
   *
   * @return
   */
  @Override
  public AddProductsResponse addProduct(@NotNull @Valid AddProductRequest addProductRequest) {

    Long id = addProductRequest.getProduct().getProductType().getId();
    ProductType productType = productTypeRepository.findById(id)
            .orElseThrow(() -> new ProductTypeNotExistException(id));

    log.info("Found productType with id: {} ", productType.getId());

    Product addedProduct = Optional.ofNullable(addProductRequest.getProduct())
            .map(productMapper::mapToAddProductDto)
            .map(product -> {
              product.setAddToWarehouseDate(LocalDate.now());
              return productRepository.save(product);
            })
            .orElseThrow(ProductNotInserted::new);

    log.info("Added product with id: {} ", addedProduct.getId());
    return AddProductsResponse.of(addedProduct.getId());
  }

  /**
   * {@inheritDoc}
   *
   * @return
   */
  @Override
  public void removeProduct(@NotNull long id) {

    if (productRepository.findById(id).isEmpty()) {
      throw new ProductNotExistException(id);
    }

    log.info("Found product with id: {}", id);

    productRepository.deleteById(id);

    log.info("Removed product with id: {} ", id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void changeProductQuantity(@NotNull @Valid ChangeProductQuantityRequest changeProductQuantityRequest) {
    Long id = changeProductQuantityRequest.getProduct().getId();

    Product product = productRepository.findById(id)
            .orElseThrow(() -> new ProductNotExistException(id));

    log.info("Found product with id: {} ", id);

    double newQuantity = changeProductQuantityRequest.getProduct().getQuantity();
    product.setQuantity(newQuantity);

    try {
      productRepository.save(product);
      log.info("Updated product with id: {} ", id);
    } catch (Exception e) {
      throw new UpdateProductQuantityError(id);
    }

  }
}