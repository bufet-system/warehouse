package com.infinitycode.bufetsystem.warehouse.exception;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Exception to mapping PRODUCT_NOT_EXIST error
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "PRODUCT_NOT_EXIST")
public class ProductNotExistException extends RuntimeException {
  private static final long serialVersionUID = 8513556996716752310L;

  @ExposeAsArg(0)
  private final long id;

  public ProductNotExistException(long id) {
    super("Product with id:" + id + " not exist");
    this.id = id;
  }
}
