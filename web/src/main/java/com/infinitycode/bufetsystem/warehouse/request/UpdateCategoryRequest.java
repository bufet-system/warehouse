package com.infinitycode.bufetsystem.warehouse.request;


import com.infinitycode.bufetsystem.warehouse.dto.category.UpdateCategoryDto;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Request model of {@link CategoryService#updateCategory(UpdateCategoryRequest)} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
@NoArgsConstructor
public class UpdateCategoryRequest {

  @Valid
  @NotNull(message = "product.required")
  @ApiModelProperty(required = true, value = "Category to update")
  private UpdateCategoryDto category;
}