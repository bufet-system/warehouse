package com.infinitycode.bufetsystem.warehouse.controller;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest;
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@AllArgsConstructor
@RequestMapping("/products")
public class ProductController {

  private final ProductService productService;

  @GetMapping(value = "/")
  @ApiOperation(value = "Return list of products in warehouse", nickname = "Return list of products in warehouse")
  @ApiImplicitParams({
          @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                  value = "Results page", defaultValue = "0"),
          @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                  value = "Number of products per page.", defaultValue = "20"),
          @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                  value = "Sorting criteria in the format: property(,asc|desc). " +
                          "Default sort order is ascending. " +
                          "Multiple sort criteria are supported.")
  })
  public GetProductsResponse getProducts(@ApiIgnore Pageable pageable) {
    return productService.getProducts(pageable);
  }

  @GetMapping("/{id}")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "[PRODUCT_NOT_EXIST] Product not exist by given id")})

  @ApiOperation(value = "Return product from warehouse with given id ", nickname = "Return product from warehouse with given id ")
  public GetProductResponse getProduct(@ApiParam(name = "id", value = "Product identifier", required = true)
                                       @NotNull @PathVariable("id") long id) {
    return productService.getProduct(id);
  }

  @PostMapping("/")
  @ApiResponses(value = {
          @ApiResponse(code = 503, message = "[PRODUCT_NOT_INSERTED] Product not inserted"),
          @ApiResponse(code = 404, message = "[PRODUCT_TYPE_NOT_EXIST] Product type not found")})
  @ApiOperation(value = "Add product", nickname = "Add product")
  public AddProductsResponse addProduct(@ApiParam(name = "Add product request.\n Return id of added product", required = true)
                                        @NotNull @Valid @RequestBody AddProductRequest addProductRequest) {
    return productService.addProduct(addProductRequest);
  }

  @DeleteMapping("/{id}")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "[PRODUCT_NOT_EXIST] Product not exist by given id")})
  @ApiOperation(value = "Remove product", nickname = "Remove product")
  public void removeProduct(@ApiParam(name = "id", value = "Product identifier", required = true)
                            @NotNull @PathVariable("id") long id) {
    productService.removeProduct(id);
  }

  @PutMapping("/quantity")
  @ApiResponses(value = {
          @ApiResponse(code = 503, message = "[UPDATE_PRODUCT_QUANTITY_ERROR ] Cannot change quantity of product by given id"),
          @ApiResponse(code = 404, message = "[PRODUCT_NOT_EXIST] Product not exist by given id")})
  @ApiOperation(value = "Change product quantity", nickname = "Change product quantity")
  public void updateProduct(@ApiParam(name = "Change product quantity request", required = true)
                            @NotNull @Valid @RequestBody ChangeProductQuantityRequest changeProductQuantityRequest) {
    productService.changeProductQuantity(changeProductQuantityRequest);
  }
}