package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Exception to mapping PARENT_CATEGORY_NOT_EXIST error
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "PARENT_CATEGORY_NOT_EXIST")
public class ParentCategoryNotExistException extends RuntimeException {
  private static final long serialVersionUID = 4476660504893537335L;

  @ExposeAsArg(value = 0)
  private final long id;

  public ParentCategoryNotExistException(long id) {
    super("Category with id: " + id + " not exist");
    this.id = id;
  }
}