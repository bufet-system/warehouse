package com.infinitycode.bufetsystem.warehouse.utils;

import lombok.experimental.UtilityClass;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.constraints.NotNull;
import java.beans.FeatureDescriptor;
import java.util.stream.Stream;

/**
 * Bufet System
 * Created by Dawid Cisowski
 * on 01.10.2019.
 */

@UtilityClass
public class BeanMergeUtils {
  /**
   * Copy properties from source to target ignoring null
   *
   * @param src    source object
   * @param target target object
   */
  public static void copyPropertiesIgnoreNull(@NotNull Object src, @NotNull Object target) {
    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
  }

  /**
   * Returning null properties from source object
   *
   * @param source source object
   * @return nullProperties[]
   */
  private static String[] getNullPropertyNames(@NotNull Object source) {
    final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
    return Stream.of(wrappedSource.getPropertyDescriptors())
            .map(FeatureDescriptor::getName)
            .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
            .toArray(String[]::new);
  }
}
