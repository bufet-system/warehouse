package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * Exception to mapping PRODUCT_QUANTITY_NOT_CHANGED error
 */

@ExceptionMapping(statusCode = SERVICE_UNAVAILABLE, errorCode = "UPDATE_PRODUCT_QUANTITY_ERROR")
public class UpdateProductQuantityError extends RuntimeException {
  private static final long serialVersionUID = 2781197698938977807L;

  public UpdateProductQuantityError(long id) {
    super("Cannot change quantity of product with id " + id);
  }
}