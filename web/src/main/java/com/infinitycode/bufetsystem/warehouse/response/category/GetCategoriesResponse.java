package com.infinitycode.bufetsystem.warehouse.response.category;


import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Response model of {@link CategoryService#getAllCategories()} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
public class GetCategoriesResponse {
  @ApiModelProperty(value = "List of all categories in warehouse")
  private List<CategoryDto> categories;
}