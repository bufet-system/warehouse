package com.infinitycode.bufetsystem.warehouse.request;

import com.infinitycode.bufetsystem.warehouse.dto.category.AddCategoryDto;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Request model of {@link CategoryService#addCategory(AddCategoryRequest) ()} service
 */
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class AddCategoryRequest {

  @Valid
  @NotNull(message = "category.required")
  @ApiModelProperty(required = true, value = "Category to add")
  private AddCategoryDto category;
}