package com.infinitycode.bufetsystem.warehouse.response.product;

import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

/**
 * Response model for {@link ProductService#getProduct(long id)} ()}
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
public class GetProductResponse {
  @ApiModelProperty(value = "Product model")
  private GetProductDto product;
}
