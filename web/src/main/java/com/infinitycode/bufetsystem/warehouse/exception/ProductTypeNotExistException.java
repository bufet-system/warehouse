package com.infinitycode.bufetsystem.warehouse.exception;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 08.08.19.
 */

import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Exception to mapping PRODUCT_TYPE_NOT_EXIST error
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "PRODUCT_TYPE_NOT_EXIST")
public class ProductTypeNotExistException extends RuntimeException {
  private static final long serialVersionUID = 5332947798831982178L;

  @ExposeAsArg(0)
  private final long id;

  public ProductTypeNotExistException(long id) {
    super("Product type with id:" + id + " not exist");
    this.id = id;
  }
}
