package com.infinitycode.bufetsystem.warehouse.service.product;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.request.ChangeProductQuantityRequest;
import com.infinitycode.bufetsystem.warehouse.response.product.AddProductsResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductResponse;
import com.infinitycode.bufetsystem.warehouse.response.product.GetProductsResponse;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Service to managing products
 */
public interface ProductService {

  /**
   * Return products
   *
   * @param pageable pagination information
   * @return response with page containing products
   */
  GetProductsResponse getProducts(Pageable pageable);


  /**
   * Return products with given id
   *
   * @param id id returning product
   * @return response with list of product{@link GetProductResponse}
   */
  GetProductResponse getProduct(@NotNull long id);

  /**
   * Add product
   *
   * @param addProductRequest request with product to add
   * @return response with added product id;
   */
  AddProductsResponse addProduct(@NotNull @Valid AddProductRequest addProductRequest);

  /**
   * Remove product
   *
   * @param id id removing product
   */
  void removeProduct(@NotNull long id);

  /**
   * Change quantity of product
   *
   * @param changeProductQuantityRequest request with quantity of product to change
   */
  void changeProductQuantity(@NotNull @Valid ChangeProductQuantityRequest changeProductQuantityRequest);
}