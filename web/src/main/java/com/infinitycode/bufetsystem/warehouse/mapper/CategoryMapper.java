package com.infinitycode.bufetsystem.warehouse.mapper;

import com.infinitycode.bufetsystem.warehouse.dto.category.AddCategoryDto;
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto;
import com.infinitycode.bufetsystem.warehouse.dto.category.UpdateCategoryDto;
import com.infinitycode.bufetsystem.warehouse.model.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import javax.validation.constraints.NotEmpty;

/**
 * MapStruct mapper to map between Category and Category DTO models
 */
@Mapper
public interface CategoryMapper {

  /**
   * Map {@link Category} model to {@link CategoryDto} model
   *
   * @param category Category model to mapping
   * @return {@link CategoryDto} model
   */
  CategoryDto mapToCategoryDto(Category category);

  /**
   * Map {@link AddCategoryDto} model to {@link Category} model
   *
   * @param addCategoryDto AddCategoryDto model to mapping
   * @return {@link Category} model
   */
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "subCategory", ignore = true)
  @Mapping(target = "productTypeListByGivenCategory", ignore = true)
  Category mapToCategory(@NotEmpty AddCategoryDto addCategoryDto);

  /**
   * Map {@link UpdateCategoryDto} model to {@link Category} model
   *
   * @param updateCategoryDto UpdateCategoryDto model to mapping
   * @return {@link Category} model
   */
  @Mapping(target = "subCategory", ignore = true)
  @Mapping(target = "productTypeListByGivenCategory", ignore = true)
  @Mapping(target = "parentCategory", ignore = true)
  Category mapToCategory(@NotEmpty UpdateCategoryDto updateCategoryDto);
}