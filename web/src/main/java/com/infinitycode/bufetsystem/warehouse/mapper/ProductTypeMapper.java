package com.infinitycode.bufetsystem.warehouse.mapper;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 08.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.producttype.AddProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.model.ProductType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * MapStruct mapper to map between ProductType and ProductType DTO models
 */
@Mapper
public interface ProductTypeMapper {

  /**
   * Map {@link ProductType} model to {@link ProductTypeDto} model
   *
   * @param productType ProductType model to mapping
   * @return {@link ProductTypeDto} model
   */
  ProductTypeDto mapToProductTypeDto(ProductType productType);

  /**
   * Map {@link ProductTypeDto} model to {@link ProductType} model
   *
   * @param productTypeDto DtoProductType model to mapping
   * @return {@link ProductType} model
   */
  @Mapping(target = "products", ignore = true)
  ProductType mapToProductType(ProductTypeDto productTypeDto);

  /**
   * Map {@link AddProductTypeDto} model to {@link ProductType} model
   *
   * @param addProductTypeDto AddProductTypeDto model to mapping
   * @return {@link ProductType} model
   */
  @Mapping(target = "products", ignore = true)
  @Mapping(target = "name", ignore = true)
  @Mapping(target = "category", ignore = true)
  ProductType mapToAddProductTypeDto(AddProductTypeDto addProductTypeDto);
}