package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Exception to mapping CATEGORY_NOT_EXIST error
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "CATEGORY_NOT_EXIST")
public class CategoryNotExistException extends RuntimeException {
  private static final long serialVersionUID = 1757271314694558849L;

  @ExposeAsArg(value = 0)
  private final long id;

  public CategoryNotExistException(long id) {
    super("Category with id: " + id + " not exist");
    this.id = id;
  }
}