package com.infinitycode.bufetsystem.warehouse.dto.product;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.model.Product;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Dto Product model for service: {@link ProductService#getProduct(long)}
 * Reference to {@link Product}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class GetProductDto {


  @ApiModelProperty(position = 1, required = true, value = "Product identifier")
  private Long id;

  @ApiModelProperty(position = 2, required = true, value = "Product type")
  private ProductTypeDto productType;

  @ApiModelProperty(position = 3, required = true, value = "Date of add product to warehouse")
  private LocalDate addToWarehouseDate;

  @ApiModelProperty(position = 4, value = "Date of expiry product date")
  private LocalDate expiryDate;

  @ApiModelProperty(position = 5, value = "Product quantity")
  private int quantity;
}
