package com.infinitycode.bufetsystem.warehouse.dto.category;

import com.infinitycode.bufetsystem.warehouse.model.Category;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Category type model.
 * Reference to {@link Category}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class CategoryDto {

  @ApiModelProperty(position = 1, value = "Category identifier")
  private Long id;

  @ApiModelProperty(position = 2, value = "Category name")
  private String name;

  @ApiModelProperty(position = 3, value = "Category description")
  private String description;

  @ApiModelProperty(position = 4, value = "Subcategory")
  private CategoryDto parentCategory;
}