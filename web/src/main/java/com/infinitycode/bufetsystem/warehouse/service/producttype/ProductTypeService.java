package com.infinitycode.bufetsystem.warehouse.service.producttype;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.response.peoducttype.GetProductTypesResponse;

/**
 * Service to managing product types
 */
public interface ProductTypeService {

  /**
   * Return list of all product types in warehouse
   *
   * @return response with list of all product types{@link GetProductTypesResponse}
   */
  GetProductTypesResponse getProductTypes();
}