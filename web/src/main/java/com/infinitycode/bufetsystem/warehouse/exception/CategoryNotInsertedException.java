package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * Exception to mapping CATEGORY_NOT_INSERTED error
 */
@ExceptionMapping(statusCode = SERVICE_UNAVAILABLE, errorCode = "CATEGORY_NOT_INSERTED")
public class CategoryNotInsertedException extends RuntimeException {

  private static final long serialVersionUID = 5124923746822683905L;

  public CategoryNotInsertedException() {
    super("Category not inserted");
  }
}