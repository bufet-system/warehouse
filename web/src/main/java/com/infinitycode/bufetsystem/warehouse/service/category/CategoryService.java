package com.infinitycode.bufetsystem.warehouse.service.category;

import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse;
import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Service to managing categories
 */
public interface CategoryService {

  /**
   * Return list of all categories in warehouse
   *
   * @return response with list of all products{@link GetCategoriesResponse}
   */
  GetCategoriesResponse getAllCategories();

  /**
   * Add category
   *
   * @param addCategoryRequest request with category to add
   * @return response with added category id
   */
  AddCategoryResponse addCategory(@NotNull @Valid AddCategoryRequest addCategoryRequest);

  /**
   * Remove category
   *
   * @param id id removing category
   */
  void removeCategory(@NotNull long id);

  /**
   * Update category
   *
   * @param updateCategoryRequest request with category to update
   */
  void updateCategory(@NotNull @Valid UpdateCategoryRequest updateCategoryRequest);
}