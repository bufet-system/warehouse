package com.infinitycode.bufetsystem.warehouse.dto.product;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 31.07.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.producttype.AddProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.model.Product;
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Dto Product model for service: {@link ProductService#addProduct(AddProductRequest)}
 * Reference to {@link Product}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
@Builder
public class AddProductDto {

  @Valid
  @NotNull(message = "productType.required")
  @ApiModelProperty(position = 1, required = true, value = "Product type")
  private AddProductTypeDto productType;

  @ApiModelProperty(position = 2, required = true, value = "Product expiry date")
  private LocalDate expiryDate;

  @NotNull(message = "quantity.required")
  @Min(value = 1, message = "Quantity must by greater than 0")
  @ApiModelProperty(position = 3, required = true, value = "Product quantity")
  private double quantity;
}
