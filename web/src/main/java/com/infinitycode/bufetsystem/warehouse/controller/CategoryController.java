package com.infinitycode.bufetsystem.warehouse.controller;

import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse;
import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@AllArgsConstructor
@RequestMapping("/category")
public class CategoryController {

  private final CategoryService categoryService;

  @GetMapping("/items")
  @ApiOperation(value = "Return list of categories in warehouse", nickname = "Return list of categories in warehouse")
  public GetCategoriesResponse getAllCategories() {
    return categoryService.getAllCategories();
  }

  @PostMapping("/item")
  @ApiResponses(value = {
          @ApiResponse(code = 503, message = "[CATEGORY_NOT_INSERTED] Category not inserted"),
          @ApiResponse(code = 400, message = "[CATEGORY_NAME_UNIQUE_ERROR] Category with given name already exist")
  })
  @ApiOperation(value = "Add category", nickname = "Add category")
  public AddCategoryResponse addCategory(@ApiParam(name = "Add category request.\n Return id of added category", required = true)
                                         @NotNull @Valid @RequestBody AddCategoryRequest addCategoryRequest) {

    return categoryService.addCategory(addCategoryRequest);
  }

  @DeleteMapping("/item/{id}")
  @Transactional
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "[CATEGORY_NOT_EXIST] Category not exist by given id"),
          @ApiResponse(code = 404, message = "[PARENT_CATEGORY_NOT_EXIST] Parent category not exist by given id"),
          @ApiResponse(code = 503, message = "[CATEGORY_IS_USED] Some product use this category")
  })
  @ApiOperation(value = "Remove category", nickname = "Remove category")
  public void removeCategory(@ApiParam(name = "id", value = "Category identifier", required = true)
                             @NotNull @PathVariable("id") long id) {
    categoryService.removeCategory(id);
  }

  @PutMapping("/item")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "[CATEGORY_NOT_EXIST] Category not exist"),
          @ApiResponse(code = 503, message = "[UPDATE_CATEGORY_ERROR] Category update error"),
          @ApiResponse(code = 400, message = "[CATEGORY_NAME_UNIQUE_ERROR] Category with given name already exist")
  })
  @ApiOperation(value = "Update category", nickname = "Update category")
  public void updateCategory(@ApiParam(name = "Update category request", required = true)
                             @NotNull @Valid @RequestBody UpdateCategoryRequest updateCategoryRequest) {
    categoryService.updateCategory(updateCategoryRequest);
  }
}