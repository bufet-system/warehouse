package com.infinitycode.bufetsystem.warehouse.utils;

import lombok.experimental.UtilityClass;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Bufet System
 * Created by Dawid Cisowski
 * on 01.10.2019.
 */

@UtilityClass
public class StreamUtils {
  /**
   * Returning sequential Stream for given Iterable<T>
   *
   * @param iterable iterable input
   * @return stream stream return
   */
  public static <T> Stream<T> toStream(Iterable<T> iterable) {
    return StreamSupport.stream(iterable.spliterator(), false);
  }
}
