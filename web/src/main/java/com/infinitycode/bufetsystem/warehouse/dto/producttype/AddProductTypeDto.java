package com.infinitycode.bufetsystem.warehouse.dto.producttype;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 06.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.model.ProductType;
import com.infinitycode.bufetsystem.warehouse.request.AddProductRequest;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Dto Product type model for service: {@link ProductService#addProduct(AddProductRequest)}
 * Reference to {@link ProductType}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class AddProductTypeDto {

  /**
   * Product type identifier
   */
  @NotNull(message = "id.required")
  @ApiModelProperty(position = 1, required = true, value = "Product type identifier")
  private Long id;
}
