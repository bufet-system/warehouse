package com.infinitycode.bufetsystem.warehouse.config;

import lombok.extern.slf4j.Slf4j;
import me.alidg.errors.ExceptionLogger;
import org.springframework.stereotype.Component;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */
@Component
@Slf4j
public class CustomExceptionLogger implements ExceptionLogger {

  @Override
  public void log(Throwable exception) {
    if (exception != null)
      log.info(exception.getLocalizedMessage());
  }
}