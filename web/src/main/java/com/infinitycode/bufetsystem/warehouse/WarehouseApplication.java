package com.infinitycode.bufetsystem.warehouse;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 09.06.19.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarehouseApplication {

  public static void main(String[] args) {
    SpringApplication.run(WarehouseApplication.class, args);
  }

}
