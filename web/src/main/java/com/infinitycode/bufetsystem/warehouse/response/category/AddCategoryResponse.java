package com.infinitycode.bufetsystem.warehouse.response.category;

import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Response model of {@link CategoryService#addCategory(AddCategoryRequest)} service
 */
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Data
public class AddCategoryResponse {

  @ApiModelProperty(required = true, value = "Id of added category")
  private Long id;
}