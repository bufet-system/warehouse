package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * Exception to mapping CATEGORY_IS_USED error
 */
@ExceptionMapping(statusCode = SERVICE_UNAVAILABLE, errorCode = "CATEGORY_IS_USED")
public class CategoryIsUsedException extends RuntimeException {
  private static final long serialVersionUID = -9194371483861324670L;

  public CategoryIsUsedException() {
    super("Category has products");
  }
}