package com.infinitycode.bufetsystem.warehouse.response.peoducttype;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.service.producttype.ProductTypeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Response model of {@link ProductTypeService#getProductTypes()} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
public class GetProductTypesResponse {
  @ApiModelProperty(value = "List of all product types in warehouse")
  private List<ProductTypeDto> productTypes;
}
