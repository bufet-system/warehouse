package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * Exception to mapping UPDATE_CATEGORY_ERROR error
 */

@ExceptionMapping(statusCode = SERVICE_UNAVAILABLE, errorCode = "UPDATE_CATEGORY_ERROR")
public class UpdateCategoryError extends RuntimeException {
  private static final long serialVersionUID = 9035944037368009557L;

  public UpdateCategoryError(long id) {
    super("Cannot update category with id " + id);
  }
}