package com.infinitycode.bufetsystem.warehouse.service.producttype;

import com.infinitycode.bufetsystem.warehouse.dto.producttype.ProductTypeDto;
import com.infinitycode.bufetsystem.warehouse.mapper.ProductTypeMapper;
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository;
import com.infinitycode.bufetsystem.warehouse.response.peoducttype.GetProductTypesResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.2019.
 */

/**
 * {@inheritDoc}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductTypeServiceImpl implements ProductTypeService {

  private final ProductTypeRepository productTypeRepository;

  private ProductTypeMapper productTypeMapper = Mappers.getMapper(ProductTypeMapper.class);

  /**
   * {@inheritDoc}
   */
  @Override
  public GetProductTypesResponse getProductTypes() {

    List<ProductTypeDto> productTypes = new ArrayList<>();
    productTypeRepository.findAll()
            .forEach(productType -> productTypes.add(productTypeMapper.mapToProductTypeDto(productType)));

    log.info("Return list of product types: {}", productTypes);

    return GetProductTypesResponse.of(productTypes);
  }
}