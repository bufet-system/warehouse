package com.infinitycode.bufetsystem.warehouse.exception;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 22.07.19.
 */

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * Exception to mapping PRODUCT_NOT_INSERTED error
 */
@ExceptionMapping(statusCode = SERVICE_UNAVAILABLE, errorCode = "PRODUCT_NOT_INSERTED")
public class ProductNotInserted extends RuntimeException {
  private static final long serialVersionUID = 4241797392834679680L;

  public ProductNotInserted() {
    super("Product not inserted");
  }
}
