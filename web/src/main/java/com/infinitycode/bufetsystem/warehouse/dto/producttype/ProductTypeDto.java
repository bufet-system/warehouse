package com.infinitycode.bufetsystem.warehouse.dto.producttype;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 17.06.19.
 */

import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto;
import com.infinitycode.bufetsystem.warehouse.model.ProductType;
import com.infinitycode.bufetsystem.warehouse.model.UnitsOfMeasurement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Product type model.
 * Reference to {@link ProductType}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class ProductTypeDto {

  /**
   * Product type identifier
   */
  @ApiModelProperty(position = 1, value = "Product type identifier")
  private Long id;

  /**
   * Product type name
   */
  @ApiModelProperty(position = 2, value = "Product type name")
  private String name;

  /**
   * Product type category
   */
  @ApiModelProperty(position = 3, value = "Product type category")
  private CategoryDto category;

  /**
   * Product  type  category
   */
  @ApiModelProperty(position = 4, value = "Product type  category")
  private UnitsOfMeasurement unitsOfMeasurement;
}