package com.infinitycode.bufetsystem.warehouse.service.category;

import com.infinitycode.bufetsystem.warehouse.dto.category.AddParentCategoryDto;
import com.infinitycode.bufetsystem.warehouse.dto.category.CategoryDto;
import com.infinitycode.bufetsystem.warehouse.mapper.CategoryMapper;
import com.infinitycode.bufetsystem.warehouse.model.Category;
import com.infinitycode.bufetsystem.warehouse.repository.CategoryRepository;
import com.infinitycode.bufetsystem.warehouse.repository.ProductTypeRepository;
import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.request.UpdateCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.response.category.AddCategoryResponse;
import com.infinitycode.bufetsystem.warehouse.response.category.GetCategoriesResponse;
import com.infinitycode.bufetsystem.warehouse.utils.BeanMergeUtils;
import com.infinitycode.bufetsystem.warehouse.exception.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor

public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;

  private CategoryMapper categoryMapper = Mappers.getMapper(CategoryMapper.class);
  private final ProductTypeRepository productTypeRepository;


  /**
   * {@inheritDoc}
   */
  @Override
  public GetCategoriesResponse getAllCategories() {

    List<CategoryDto> categories = new ArrayList<>();

    categoryRepository.findAll()
            .forEach(category -> categories.add(categoryMapper.mapToCategoryDto(category)));

    log.info("Return list of categories: {}", categories);

    return GetCategoriesResponse.of(categories);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AddCategoryResponse addCategory(@NotNull @Valid AddCategoryRequest addCategoryRequest) {

    String categoryName = addCategoryRequest.getCategory().getName();

    categoryRepository.findByName(categoryName).stream()
            .findAny()
            .ifPresent(category -> {
              throw new CategoryNameUniqueError();
            });

    log.info("Category name is unique");

    if (addCategoryRequest.getCategory().getParentCategory() != null) {
      validateParentCategory(addCategoryRequest.getCategory().getParentCategory());
    }

    Category addedCategory = Optional.ofNullable(addCategoryRequest.getCategory())
            .map(categoryMapper::mapToCategory)
            .map(categoryRepository::save)
            .orElseThrow(CategoryNotInsertedException::new);

    log.info("Added category with id: {}", addedCategory.getId());

    return AddCategoryResponse.of(addedCategory.getId());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeCategory(@NotNull long id) {
    Category category = categoryRepository.findById(id)
            .orElseThrow(() -> new CategoryNotExistException(id));

    log.info("Found category with id: {}", id);

    categoryUsedValidation(category);
    log.info("Category with id: {} is not used", id);

    categoryRepository.deleteById(id);
    log.info("Deleted category with id: {}", id);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateCategory(@NotNull @Valid UpdateCategoryRequest updateCategoryRequest) {

    Category categoryToUpdate = categoryMapper.mapToCategory(updateCategoryRequest.getCategory());

    Category foundedCategory = categoryRepository.findById(categoryToUpdate.getId())
            .orElseThrow(() -> new CategoryNotExistException(categoryToUpdate.getId()));

    BeanMergeUtils.copyPropertiesIgnoreNull(categoryToUpdate, foundedCategory);

    log.info("Found category with id: " + foundedCategory.getId());

    String categoryName = updateCategoryRequest.getCategory().getName();
    validateCategoryName(categoryName, foundedCategory.getId());
    try {
      Category category = categoryRepository.save(foundedCategory);
      log.info("Updated category with id:" + category.getId());

    } catch (Exception e) {
      throw new UpdateCategoryError(categoryToUpdate.getId());
    }
  }

  private void validateCategoryName(String categoryName, Long id) {
    List<Category> categories = categoryRepository.findByName(categoryName);

    categories.stream()
            .filter(category -> !category.getId().equals(id))
            .findAny()
            .ifPresent(s -> {
              throw new CategoryNameUniqueError();
            });
  }

  private void validateParentCategory(@NotNull @Valid AddParentCategoryDto parentCategoryDto) {
    Category category = categoryRepository.findById(parentCategoryDto.getId())
            .orElseThrow(() -> new ParentCategoryNotExistException(parentCategoryDto.getId()));

    log.info("Found parent category with id: {}", category.getId());
  }

  private void categoryUsedValidation(Category category) {
    if (hasSubcategory(category) || !productTypeRepository.findProductTypesByCategory(category).isEmpty()) {
      throw new CategoryIsUsedException();
    }
  }

  private boolean hasSubcategory(Category category) {
    if (category.getSubCategory() != null) {
      return !category.getSubCategory().isEmpty();
    }
    return false;
  }
}