package com.infinitycode.bufetsystem.warehouse.controller;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 19.08.19.
 */

import com.infinitycode.bufetsystem.warehouse.response.peoducttype.GetProductTypesResponse;
import com.infinitycode.bufetsystem.warehouse.service.producttype.ProductTypeService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productType")
@RequiredArgsConstructor
public class ProductTypeController {

  private final ProductTypeService productTypeService;

  @GetMapping("/items")
  @ApiOperation(value = "Return list of product types in warehouse", nickname = "Return list of product types in warehouse")
  public GetProductTypesResponse getProducts() {
    return productTypeService.getProductTypes();
  }
}