package com.infinitycode.bufetsystem.warehouse.dto.category;

import com.infinitycode.bufetsystem.warehouse.request.AddCategoryRequest;
import com.infinitycode.bufetsystem.warehouse.service.category.CategoryService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Dto Category type model for service: {@link CategoryService#addCategory(AddCategoryRequest)}
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel
public class AddParentCategoryDto {


  @ApiModelProperty(position = 1, required = true, value = "Category identifier")
  @NotNull(message = "parentCategoryIdentifier.required")
  private Long id;
}