package com.infinitycode.bufetsystem.warehouse.response.product;

import com.infinitycode.bufetsystem.warehouse.dto.product.GetProductDto;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 16.03.19.
 */

/**
 * Response model for {@link ProductService#getProducts(Pageable pageable)}
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
public class GetProductsResponse {
  @ApiModelProperty(value = "Page witch products")
  private Page<GetProductDto> productsPage;
}