package com.infinitycode.bufetsystem.warehouse.request;


import com.infinitycode.bufetsystem.warehouse.dto.product.ChangeProductQuantityDto;
import com.infinitycode.bufetsystem.warehouse.service.product.ProductService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Request model of {@link ProductService#changeProductQuantity(ChangeProductQuantityRequest)} ()} service
 */
@Data
@AllArgsConstructor(staticName = "of")
@ApiModel
@NoArgsConstructor
public class ChangeProductQuantityRequest {

  @Valid
  @NotNull(message = "product.required")
  @ApiModelProperty(required = true, value = "Product to change quantity")
  private ChangeProductQuantityDto product;
}
