package com.infinitycode.bufetsystem.warehouse.exception;

import me.alidg.errors.annotation.ExceptionMapping;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Exception to mapping CATEGORY_NAME_UNIQUE_ERROR error
 */
@ExceptionMapping(statusCode = BAD_REQUEST, errorCode = "CATEGORY_NAME_UNIQUE_ERROR")
public class CategoryNameUniqueError extends RuntimeException {

  private static final long serialVersionUID = 6937519887216223739L;


  public CategoryNameUniqueError() {
    super("Category with given name already exist");

  }
}