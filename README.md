# Warehouse Service

## About

* Part of Buffet Ecosystem. Microservice responsible for managing warehouse in Buffet System. 
* Project status: developing

See more information in dedicated [**Buffet System Documentation**](https://buffet-system.slite.com/p/channel/APTkT8s5sBN7ktqGKVZhfh)
## Table of contents

> * [Title / Repository Name](#title--repository-name)
>   * [About / Synopsis](#about--synopsis)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>   * [Usage](#usage)
>   * [Build](#build)
>   * [Deploy (how to install build product)](#deploy-how-to-install-build-product)
>   * [License](#license)

## Installation

Sample:
Warehouse service is able to install in three ways:
- From Intellij Idea (Recommended Ide):   
    `Run Main class (WarehouseApplication) as Spring Boot Application`
- From Jar:
  * Build Maven project: `mvn clean install`
  * Go to target directory: `cd web\target`
  * Run java app `java -jar bufet_warehouse_app.jar`    
- As Docker container
  * Run docker container: `docker run -d --name=warehouse --publish=8090:8090 registry.gitlab.com/bufet-system/warehouse:[specyfic version]`
- As Helm package: (Future) docker run -d --name=warehouse --publish=8090:8090 warehouse:latest
## Usage
Dictionary service share Rest Api described in Swagger:
Link to Stagging Swagger-UI: [http://www.api.buffet-system-staging.space:30670/warehouse/swagger-ui.html](http://www.api.buffet-system-staging.space:30670/warehouse/swagger-ui.html)


### Build

    mvn clean install

### Deploy

To Deploy service to environments we use CI : 
* Gitlab CI
* Kubernetes
* Helm
     
## License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
